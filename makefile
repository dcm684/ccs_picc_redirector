# Makes the CCS PICC Redirector for use with old PICC compilers
SOURCES=redirect.c
EXECUTABLE=redirect.exe

all: release
	
release:
	gcc -o $(EXECUTABLE) -O3 $(SOURCES)
	"C:\Program Files\7-Zip\7z.exe" a -tzip -mx9 Redirector.zip README.txt $(EXECUTABLE)
	
#For use with gdb
debug:
	gcc -o $(EXECUTABLE) -ggdb $(SOURCES)