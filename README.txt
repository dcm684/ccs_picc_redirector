About:
This redirector program solves the following errors in MPLABX when using 
older version of the CCS PICC compiler.

Invalid option: out=dist/default/production

Invalid option: +EXPORTD=dist/default/production

MPLABX likes its output to be in a given directory. Recent CCS PICC compiler
versions accomodate this with the "EXPORTD" and "out" command line arguments.
This program augements the older compilers with the newer functionality.

Older compilers do not seem to like running when MAKEFILE is in
a different directory than the main source file. This program does not
cause or solve that problem.

Author has no relationship with CCS, Inc.

Usage:
1) Go the compiler directory, e.g. C:\Program Files\PICC\.
2) Rename the existing PICC compiler, e.g. "ccsc.exe", to "real_ccsc.exe".
3) Place the included "redirect.exe" into the same folder as the compiler.
4) Rename "redirect.exe" to the compiler's oringal name, e.g. "ccsc.exe".
5) Compile using MPLABX.

Program can still be used with MPLAB 8. All input will just be passed along
to the compiler since no files will be moved.

Additional Arguments:
+DISPERR	If MPLABX will not display errors in the code being compiled,
			add this arguments to the compiler arguments. This will cause
			the contents of the err file to be sent to stdout. This
			argument will be stripped before sending the compile string
			to the CCS compiler.

License:
Copyright (c) 2012, Christopher Meyer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

Neither the name of the project nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.