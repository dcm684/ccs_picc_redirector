/**
 * Copyright (c) 2012, Christopher Meyer
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the project nor the names of its contributors may
 * be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */

/**
 * MPLABX likes its output to be in a given directory. Recent CCS PICC
 * compiler versions accomodate this with the "EXPORTD" and "out" command
 * line arguments. This program augements the older compilers with the
 * newer functionality.
 *
 * Program can still be used with MPLAB 8. All input will just be passed
 * along to the compiler since no files will be moved.
 *
 * Older compilers do not seem to like running when MAKEFILE is in
 * a different directory than the main source file. This program does not
 * cause or solve that problem.
 *
 * @author Christopher Meyer
 * @date 2012 June 27
 */

#include <stdio.h>
#include <string.h>

#ifdef __unix__
#include <unistd.h>
#elif defined _WIN32
#include <windows.h>
#define sleep(x) Sleep(1000 * x)
#endif

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>

/* For dir listing and moving files */
#include <w32api.h>
#include <dirent.h>

/* For mod time */
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

/**
 * Provides the UNIX modified time of the given file
 *
 * @param fileName File to get modified time
 * @param modTime Pointer to where to store the time
 * @return Zero if the check was successful, -1 if it wasn't
 */
int getModTime(char* fileName, time_t* modTime) {
    struct stat fileStat;
    int retVal = 0;

    /* Check if the file exists, then get its mod time */
    if (!stat(fileName, &fileStat)) {
        *modTime = fileStat.st_mtime;
    } else {
        retVal = -1;
    }

    return retVal;
}

/**
 * Since Windows accepts both / and \ as directory seperators this
 * function changes all / to \.
 *
 * @param inString String to modify
 */
void replace_backslash(char* inString) {
    char* index;
    index = inString + 1;
    while(*index != '\0') {
        if(*index == '\\') {
            *index = '/';
        }
        index++;
    }
}

/**
 * Similar to mkdir but will create all of the missing directories. Doesn't
 * check for files. Assumes all input is a directory.
 *
 * @param inPath Directory tree to create
 * @return 0 if the creation was successful, -1 if it wasn't. errno is
 *      populated with the mkdir errors if it wasn't successful
 */
int mkdir_recurse(char* inPath) {
    char* localPath;
    char* index;

    /* Make a local copy of the path so you can modify it without effecting
    the original */	
    localPath = malloc(strlen(inPath) + 1);
    localPath = strcpy(localPath, inPath);

    replace_backslash(localPath);

    /* Remove the trailing slash since the last thing done will be create the
    last directory */
    if(localPath[strlen(localPath) - 1] == '/') {
        localPath[strlen(localPath) - 1] = 0;
    }

    /* Until a null char is found keep on making directories */
    index = localPath + 1;
    while(*index != '\0') {
        /* When a slash is found make it a null char to trick mkdir into
        thinking that all you want to create is the path up until the slash.
        Replace the slash after creating the directory */
        if(*index == '/') {
            *index = 0;
            if ((mkdir(localPath) != 0) &&
                (errno != EEXIST)) {
                return -1;
            }

            *index = '/';
        }
        index++;
    }

    /* Make the final directory */
    if ((mkdir(localPath) != 0) &&
        (errno != EEXIST)) {
        return -1;
    }

    return 0;
}

/**
 * Moves files modified after the given time and those with names that
 * start with the given prefix
 *
 * @param inputDir Path where the files will be moved from
 * @param destination Path where the files will be moved to
 * @param inTime All files with the correct prefix modifed at or after
 *	this time will be moved
 * @param prefix Prefix that all files must have before they are moved.
            Set to NULL if all prefixes are allowed.
 * @param suffix Suffix that all files must have before they are moved.
            Set to NULL if all suffixes are allowed. If you want all
            files with a certain extension to be moved, make sure that
            there is a '.' before the extension.
 */
int moveRecentFiles(const char *inputDir, char* destination, time_t inTime,
                        char* prefix, char* suffix) {
    struct dirent *entry;
    DIR *dp;
    DIR *checkedDir;

    char* inFileName;
    char* outFileName;

    int fileNameLength;

    time_t modTime;

    /* Create the destination directory if it does not exist */
    if(mkdir_recurse(destination) != 0) {
        if(errno != EEXIST) {
            printf("\r\n%s. %s", strerror(errno), destination);
            return -1;
        }
    }

    /* Open the directory for listing the file names */
    dp = opendir(inputDir);
    if (dp == NULL) {
        printf("\r\nFailed to Open Input Directory, \"%s\"", inputDir);
        return -1;
    }

    while((entry = readdir(dp))) {
        checkedDir = opendir(entry->d_name);
        if(checkedDir == NULL) {
            getModTime(entry->d_name, &modTime);

            fileNameLength = strlen(entry->d_name);

            /* Only move recent files that have the prefix given and are
            not '.' dirs.
            Never move header (.h) or source files (.c) either */
            if ((modTime >= inTime) &&
                ((entry->d_name)[0] != '.') &&
                (strcmp(".c", (entry->d_name) + (fileNameLength - 2) * sizeof(char)) != 0) &&
                (strcmp(".cpp", (entry->d_name) + (fileNameLength - 4) * sizeof(char)) != 0) &&
                (strcmp(".h", (entry->d_name) + (fileNameLength - 2) * sizeof(char)) != 0) &&
                ((prefix == NULL) ||
                    (strncmp(prefix, entry->d_name, strlen(prefix)) == 0)) &&
                ((suffix == NULL) ||
                    ((fileNameLength >= strlen(suffix)) &&
                    (strcmp(suffix, (entry->d_name) + (fileNameLength - strlen(suffix)) * sizeof(char)) == 0)))) {

                /* Append the path to the input and output file names */
                inFileName = malloc((strlen(inputDir) + fileNameLength + 2) * sizeof(char));
                inFileName = strcpy(inFileName, inputDir);
                inFileName = strcat(inFileName, "/");
                inFileName = strcat(inFileName, (entry->d_name));

                outFileName = malloc((strlen(destination) + fileNameLength + 2) * sizeof(char));
                outFileName = strcpy(outFileName, destination);
                outFileName = strcat(outFileName, "/");
                outFileName = strcat(outFileName, (entry->d_name));

                /* Move the file, overwritting the old one if necessary */
                //if(MoveFileEx(inFileName, outFileName, MOVEFILE_REPLACE_EXISTING) == 0) {
                remove(outFileName); /* Ignore if there's an error. Error 
                                        could get thrown if the file doesn't
                                        already exist. */
                
                if(rename(inFileName, outFileName) != 0) {
                    printf("\r\nFailed to move, \"%s\", to \"%s\"", inFileName, outFileName);
                }
                free(inFileName);
                free(outFileName);
            }
        } else {
            closedir(checkedDir);
        }
    }

    closedir(dp);

    free(entry);
    free(dp);
    return 0;
}

/**
 * Goes through a given argument and determines what is to be done to it.
 */
void ParseArguments(char** inArg, char** passedArgs,
    int* currentPassedArgsLen, int* passedArgBufferSize, int additionalPAIncs,
    char** outputArgPrefix, char** outLocation, 
    char** objDirArgPrefix, char** objLocation,
    char** sourceFileDir, char** sourceFileName,
    char** dispErrString, bool* displayErrFile) {
    
    int outputArgPrefixLen; /* Number of characters in the output argument */
    int objDirArgPrefixLen; /*<Length of the text in objDirArgPrefix */
    int dispErrStringLen; /*<Length of the text in dispErrString */
    static char includeArgs[][4] = {"I+=", "I="}; /*<Argument for including
                                                on the command line.
                                                Params must start with
                                                the same letter */
    static int includeArgsArrLen = 2;   /*<Items in the includeArgs array */

    outputArgPrefixLen = strlen(*outputArgPrefix);
    objDirArgPrefixLen = strlen(*objDirArgPrefix);
    dispErrStringLen = strlen(*dispErrString);
    
        
    //printf("ParseArgs: %s\r\n", *inArg);

    /* Output directory */
    if (strncasecmp(*inArg, *outputArgPrefix, outputArgPrefixLen) == 0) {
        *outLocation = malloc((strlen(*inArg) - outputArgPrefixLen + 1) * sizeof(char));
        *outLocation = strcpy(*outLocation, *inArg + outputArgPrefixLen);
    /* Object directory */
    } else if (strncasecmp(*inArg, *objDirArgPrefix, objDirArgPrefixLen) == 0) {
        *objLocation = malloc((strlen(*inArg) - objDirArgPrefixLen + 1) * sizeof(char));
        *objLocation = strcpy(*objLocation, *inArg + objDirArgPrefixLen);
    /* Print err file contents */
    } else if (strncasecmp(*inArg, *dispErrString, dispErrStringLen) == 0) {
        *displayErrFile = true;
    } else {
        /* Make sure there is enough space for a trailing
        space (+1), quotes (+2), and null term */
        *currentPassedArgsLen = *currentPassedArgsLen + strlen(*inArg) + 4;
        while (*currentPassedArgsLen > *passedArgBufferSize) {
            *passedArgBufferSize = *passedArgBufferSize + additionalPAIncs;
            *passedArgs = realloc(*passedArgs, *passedArgBufferSize * sizeof(char));
        }
        *passedArgs = strcat(*passedArgs, " ");

        /* Is the paramater an include location? */
        if(*inArg[0] == includeArgs[0][0]) {
            int j;
            for (j = 0; j < includeArgsArrLen; j++) {
                if(strncasecmp(*inArg, includeArgs[j], strlen(includeArgs[j])) == 0) {
                    *passedArgs = strncat(*passedArgs, *inArg, strlen(includeArgs[j]));
                    *passedArgs = strcat(*passedArgs, "\"");
                    *passedArgs = strcat(*passedArgs, *inArg + strlen(includeArgs[j]));
                    *passedArgs = strcat(*passedArgs, "\"");
                }
            }
        /* Is this the source file. Assumes the source file is
        the only path that's a path and not an include */
        } else if ((strstr(*inArg, "./") > 0) ||
        (strstr(*inArg, ".\\") > 0) ||
        (strstr(*inArg, ".c") > 0) ||
        (strstr(*inArg, ".h") > 0) ||
        (strstr(*inArg, ".o") > 0)
        ) {

            int lastSlash = 0;

            *sourceFileDir = malloc((strlen(*inArg) + 1) * sizeof(char));
            *sourceFileDir = strcpy(*sourceFileDir, *inArg);

            replace_backslash(*sourceFileDir);

            *sourceFileName = malloc((strlen(*sourceFileDir) + 1) * sizeof(char));
            *sourceFileName = strcpy(*sourceFileName, *sourceFileDir);

            /* Look for the last / and copy everything before it to
            the directory variable and copy everything after to the
            file name */
            if (strrchr(*inArg, '/') != NULL) {
                lastSlash = strrchr(*inArg, '/') - *inArg;
                *sourceFileName = strcpy(*sourceFileName, strrchr(*sourceFileName, '/') + 1);
            }
            *sourceFileDir[lastSlash] = '\0';

            /* In the case that no directory is attached given with
            the file name, set the directory to '.' in order to let
            later functions work properyl, eg. directory listing */
            if(strlen(*sourceFileDir) == 0) {
                *sourceFileDir = realloc(*sourceFileDir, 2);
                *sourceFileDir = ".";
            }

            /* Strip the extension from the *sourceFileName */
            if (strrchr(*sourceFileName, '.') != NULL) {
                (strrchr(*sourceFileName, '.'))[0] = '\0';
            }

            /* Take the source file back onto the arguments,
            surrounding it with quotes */
            *passedArgs = strcat(*passedArgs, "\"");
            *passedArgs = strcat(*passedArgs, *inArg);
            *passedArgs = strcat(*passedArgs, "\"");

        } else {
            /* Every other parameter just pass along */
            *passedArgs = strcat(*passedArgs, *inArg);
        }
    }
}

/**
 * Checks if a path is absolute and if it isn't creates one using the given
 * current path
 *
 * @param inPath Path to modify
 * @param currentPath Path to append inPath to if it isn't absolute
 *
 * @return Absolute path
 */
char* appendPath(char* inPath, char* currentPath) {
    if((1 != (strstr(inPath, ":\\") - inPath)) &&
        (1 != (strstr(inPath, ":/") - inPath)) &&
        (inPath[0] != '/')
    ) {
        char* stringTemp;
        
        stringTemp = malloc(strlen(inPath) + strlen(currentPath) + 2);
        stringTemp = strcpy(stringTemp, currentPath);
        stringTemp = strcat(stringTemp, "/");
        stringTemp = strcat(stringTemp, inPath);
        inPath = malloc(strlen(stringTemp) + 1);
        inPath = strcpy(inPath, stringTemp);

        free(stringTemp);
    }
    //printf("iP: %s", inPath);
    
    return inPath;
}

/**
 * Goes through the string of arugments to be passed on and moves +EA,
 * +EW, and -EW to the end of the string.
 *
 * When +EO or +ES are after the others some compilers mess up and act
 * like -E is set and quit after the first error. When they are before
 * them it works fine.
 *
 * @param inArgString The string of arguments that will be searched and
 * fixed if necessary
 */
void fixErrorPrinting(char **inArgString) {
    static char errorShowArr[][5] = {" +EA", " +EW", " -EW"};
    char *stringPos;
    int i = 0;
    char *stringTemp;
    
    stringTemp = malloc(strlen(*inArgString) + 5);
    stringTemp = strcpy(stringTemp, *inArgString);
    //printf("Init: %s\r\n\n", stringTemp);
    
    for(i = 0; i < 3; i++) {
        stringPos = strstr(stringTemp, errorShowArr[i]);
        if(stringPos != NULL) {
            while(stringPos != NULL) {
                while(*(stringPos + 4) != '\0') {
                    *stringPos = *(stringPos + 4);
                    stringPos++;
                }
                *stringPos = '\0';
                stringPos = strstr(stringTemp, errorShowArr[i]);
            }            
            stringTemp = strcat(stringTemp, errorShowArr[i]);
        }
        //printf("%s: %s\r\n\n", errorShowArr[i], stringTemp);
    }
    *inArgString = strcpy(*inArgString, stringTemp);
}

/**
 * Parses the compiler execute command from MPLABX and coverts into
 * something that can be used by older CCS PICC compilers. The old compiler
 * file, must be renamed to "real_ccsc.exe" for this to work. This file
 * must be named what the compiler was named before.
 *
 * The newer, MPLABX compatible CCS PICC compilers have additional output
 * arguments. This includes "out=" and "+EXPORTD=". This program will strip
 * those arguments from the command when executing the older compilers. Once
 * the older compiler has run, this program will move the output to the
 * directory specified by the "out=" argument.
 *
 * "+EXPORTD=" is not documented in the PICC manual asince it is a new
 * argument, but from projects found on the Internet this is where object
 * files are to be moved, so that's where .o files will be moved. In the
 * makefile found online, "out" and "exportd" were set to the same value,
 * "${OBJECTDIR}".
 *
 * @param argc Number of arguments given at the command line.
 * @param argv Arguments given at the command line. Argument 0
 *      is this file's name
 * @return Zero if the operation was successful. One if it wasn't.
 */
int main(int argc, char *argv[]){

    char* dispErrString = "+DISPERR"; /*< Parameter indicating that the 
                                        the contents of the err file are to
                                        be dispalyed? */
    bool dispErrFile = false;   /*<The contents of the err file will be
                                    displayed */

    char* compilerFile = "real_ccsc.exe"; /*<Name of the real compiler */
    char* compilerPath = NULL; /*<Path given to the compiler. Might be
                                nothing if the compiler was called from
                                its directory */

    static char *outputArgPrefix = "out="; /*<Argument used to specify the
                                            output directory */
    char* outLocation = NULL; /* Where the compiler output will be moved to */

    static char *objDirArgPrefix = "+EXPORTD="; /*<No idea what this argument
                                                    does, but the old compiler
                                                    doesn't like it. Has
                                                    something to do with .o
                                                    files */

    char* objLocation = NULL; /*<Where the object files will be moved to */
    static char* objFileExt = ".o"; /*<Extension for object files*/

    int passedArgBufferSize = 100;    /*<Initial number of chars in the
                                        passed arguments */
    static int additionalPAIncs = 100; /*<Amount to increment the passed
                                        arguments value by when no more
                                        space is available */
    char* passedArgs;    /*<Where the passed arguments are stored */

    char* outCommand;   /*<Stripped compiler command with arguments */

    char* sourceFileDir;    /*<Directory of the source file being
                                compiled */
    char* sourceFileName;   /*<Name of the source file being
                                compiled */

    int retVal = 0;         /*<Value returned at the end of execution */

    time_t currentTime;     /*<Time the compile started */
    
    /* Set the passed argument string to empty */
    passedArgs = malloc(passedArgBufferSize * sizeof(char));
    passedArgs[0] = '\0';

    /* Set the path to real compiler */
    compilerPath = malloc((strlen(argv[0]) + strlen(compilerFile) + 3) * sizeof(char));
    compilerPath = strcpy(compilerPath, "\"");
    compilerPath = strcat(compilerPath, argv[0]);
    replace_backslash(compilerPath);
    if(strchr(compilerPath, '/') != NULL) {
        /* Just overwrite this file name, don't update the address. Enough
        space has been malloced that there is no worry about hitting the
        end of the space */
        strcpy(strrchr(compilerPath, '/') + 1, compilerFile);
    } else {
        compilerPath = strcpy(compilerPath, "\"");
        compilerPath = strcat(compilerPath, compilerFile);
    }
    compilerPath = strcat(compilerPath, "\"");
    
    //LoadLibraryA("backtrack.dll");
    if (argc > 1) {    

        int currentPassedArgsLen = 0;      /*<Number of characters being
                                            passed to the compiler so far */
        int i;
        char* quotePosition;
        char* equalPosition;
        char* currentChar;
        char* lastChar;
        
        for(i = 1; i < argc; i++) {
            //printf("argv[%u]: %s\r\n", i, argv[i]);
            
            
            /* Look for a stray quote. All first quotes in a parameter need to be
            preceded by an '='. Stray quotes will cause the arguments to get 
            mixed up */
            quotePosition = strchr(argv[i], '"');
            if(quotePosition != NULL) {
                equalPosition = strstr(argv[i], "=\"");
                if((equalPosition == NULL) || !(equalPosition == quotePosition - 1)) {
                    //printf("%u <q>%s</q>\r\n", quotePosition, argv[i]);
                    *quotePosition = ' ';
                    lastChar = argv[i];
                    currentChar = argv[i];
                    /* Find each space and treat it as a seperator between args */
                    while(*currentChar != '\0') {
                        if(*currentChar == ' ') {
                            *currentChar = '\0';
                            //printf("BOO (%u)", lastChar[0]);
                            if(strlen(lastChar) > 0) {
                                ParseArguments(&lastChar, &passedArgs,
                                    &currentPassedArgsLen, &passedArgBufferSize, additionalPAIncs,
                                    &outputArgPrefix, &outLocation, 
                                    &objDirArgPrefix, &objLocation,
                                    &sourceFileDir, &sourceFileName,
                                    &dispErrString, &dispErrFile);
                            }
                            lastChar = currentChar + 1;
                        }
                        currentChar++;
                    }
                    ParseArguments(&lastChar, &passedArgs,
                        &currentPassedArgsLen, &passedArgBufferSize, additionalPAIncs,
                        &outputArgPrefix, &outLocation, 
                        &objDirArgPrefix, &objLocation,
                        &sourceFileDir, &sourceFileName,
                        &dispErrString, &dispErrFile);
                }
            } else {
            
            ParseArguments(&argv[i], &passedArgs,
                &currentPassedArgsLen, &passedArgBufferSize, additionalPAIncs,
                &outputArgPrefix, &outLocation, 
                &objDirArgPrefix, &objLocation,
                &sourceFileDir, &sourceFileName,
                &dispErrString, &dispErrFile);
            }
        }
        
        fixErrorPrinting(&passedArgs);
    }

    /* Put together the call to the compiler */
    outCommand = malloc((strlen(passedArgs) + strlen(compilerPath) + 10) * sizeof(char));
    /* Since system is lame and cannot handle a leading quote, put an
    always true function at the beginning of the command */
    outCommand = strcpy(outCommand, "if 1==1 ");
    outCommand = strcat(outCommand, compilerPath);
    outCommand = strcat(outCommand, passedArgs);
    //outCommand = strcat(outCommand, " 2>&1");
    

    /* Get the current time so you know what files were modified during
    the compile */
    currentTime = time(NULL);

    /* Run the commands */
	//printf("\r\n%s\n", outCommand);
    retVal = system(outCommand);
    //printf("Retval %u", retVal);
    
    if(argc > 1) {
        
        /* Displa the contents of the err file if the user so desires */
        if(dispErrFile) {
            char c;
            char *errFileName;
            FILE *file;
            
            /* Create the err file name. Add 6 for the slash, .err, and null
                terminator */
            errFileName = malloc(strlen(sourceFileDir) + 
                                    strlen(sourceFileName) + 6);
            errFileName = strcpy(errFileName, sourceFileDir);
            errFileName = strcat(errFileName, "/");
            errFileName = strcat(errFileName, sourceFileName);
            errFileName = strcat(errFileName, ".err");
            
            file = fopen(errFileName, "r");
            if (file) {
                while ((c = getc(file)) != EOF)
                    putchar(c);
                fclose(file);
            }
        }
        
        /* Move the object files so sourceFile.o won't be moved when
            sourceFile.lst, sourceFile.hex and their ilk are moved */
        if ((objLocation != NULL) && (strlen(objLocation) > 0)) {
            /* Create the string sourceFileDir/objLocation */
            outLocation = appendPath(objLocation, sourceFileDir);

            retVal = moveRecentFiles(sourceFileDir, objLocation,
                                        currentTime, NULL, objFileExt);
            if(retVal == -1) {
                printf("\r\nObject Move Failed");
            }
        }

        /* Move sourceFile.lst, sourceFile.hex and their ilk */
        if ((outLocation != NULL) && (strlen(outLocation) > 0)) {
            /* Create the string sourceFileDir/outLocation */
            outLocation = appendPath(outLocation, sourceFileDir);

            retVal = moveRecentFiles(sourceFileDir, outLocation,
                                        currentTime, sourceFileName, NULL);
            if(retVal == -1) {
                printf("\r\nOutput Move Failed");
            }
        }
        
        #ifdef THROW_ERROR_IF_ERR_FILE_IS_CREATED 
        //If no errors but warnings are found, this would kill compile
        /* See if an error file exists and if it was created after the 
        compile started */
        char* stringTemp;       /*<Holder for modifying strings */
        if ((outLocation != NULL) && (strlen(outLocation) > 0)) {
            stringTemp = malloc(strlen(outLocation) + strlen(sourceFileName) + 6);
            stringTemp = strcpy(stringTemp, outLocation);
            stringTemp = strcat(stringTemp, "/");
        } else {
            stringTemp = malloc(strlen(sourceFileName) + 5);
            stringTemp[0] = '\0';
        }
        stringTemp = strcat(stringTemp, sourceFileName);
        stringTemp = strcat(stringTemp, ".err");
        if(_access(stringTemp, 00) == 0) {
            time_t modTime;
            getModTime(stringTemp, &modTime);
            if(modTime >= currentTime) {
                retVal = -1;
            }
        }
        free(stringTemp);
        #endif
        
        if (outLocation != NULL) {
            free(outLocation);
        }
        if (objLocation != NULL) {
            free(objLocation);
        }        
        if (sourceFileDir != NULL) {
            free(sourceFileDir);
        }        
        if (sourceFileName != NULL) {
            free(sourceFileName);
        }
        
    } else {
        printf(" \
            PIC-C Redirector, developed by Christopher Meyer\r\n\n\
            Takes all arguments sent to the CCS compiler and passes them on.\r\n\
            \r\n\
            Additional Arguments:\r\n\
            %s - Prints the contents of the err file", 
            dispErrString
        );
    }
    
    //sleep(10);

    free(passedArgs);
    free(outCommand);
    free(compilerPath);
    
    return retVal;
 }
